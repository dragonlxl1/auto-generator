package com.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: lxl
 * @time: 2021/3/27 19:43
 */
@SpringBootApplication(scanBasePackages = "com.generator")
@MapperScan(basePackages = "com.generator")
public class GeneratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorApplication.class,args);
    }
}
