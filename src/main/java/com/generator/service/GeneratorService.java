package com.generator.service;

public interface GeneratorService {
    /**
     * 生成代码
     */
    byte[] generatorCode(String[] tableNames,String packageName);
}
