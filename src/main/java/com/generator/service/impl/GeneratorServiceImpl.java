package com.generator.service.impl;

import com.generator.dao.SysGeneratorDao;
import com.generator.entity.ResultMap;
import com.generator.service.GeneratorService;
import com.generator.utils.Constant;
import com.generator.utils.GenUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @description:
 * @author: lxl
 * @time: 2021/3/27 21:42
 */

@Service("GeneratorServiceImpl")
public class GeneratorServiceImpl implements GeneratorService {

    @Autowired
    private SysGeneratorDao sysGeneratorDao;

    @Override
    public byte[] generatorCode(String[] tableNames,String packageName) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);

        for (String tableName : tableNames) {
            //查询表信息
            Map<String, String> table = queryTable(tableName);
            //查询列信息
            List<Map<String, String>> columns = queryColumns(tableName);
            //生成代码
            GenUtils.generatorCode(table, columns, zip,packageName);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }
    public Map<String, String> queryTable(String tableName) {

        return sysGeneratorDao.queryTable(tableName);
    }
    public List<Map<String, String>> queryColumns(String tableName) {

        return sysGeneratorDao.queryColumns(tableName);
    }

}
